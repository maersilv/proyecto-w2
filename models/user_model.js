const mongoose = require('mongoose');
const schema =  mongoose.Schema;

const user = new schema({
    name : {type: String},
    lastname : {type:String},
    date_of_birth: {type:Date},
    email : {type:String},
    country : {type:String},
    cellphone: {type:Number},
    password : {type:String},
    con_password : {type:String},
    state:{type:Boolean},
    codeSms:{type:Number}
});

module.exports= mongoose.model('user',user);