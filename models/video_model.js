const mongoose = require('mongoose');
const schema = mongoose.Schema;

const video = new schema({
    id_user: { type: String },
    name: { type: String },
    url: { type: String },
});


module.exports = mongoose.model('video', video);