const mongoose = require('mongoose');
const schema =  mongoose.Schema;

const profile = new schema({
    id_user:{type:String},
    name : {type: String},
    username : {type:String},
    pin:{type:Number},
    age:{type:Number},
});

module.exports= mongoose.model('profile',profile);