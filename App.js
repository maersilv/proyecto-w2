var express = require('express');
var app = express();
var cors = require('cors');

//agrega la conexion
const database = require('./config/database.js');
//agrega el driver de mongo
const mongoose = require('mongoose');
//agrega el parseardor
let bodyParser = require('body-parser');
//agrega cors para peticiones
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));



//conecta la base de datos
mongoose.connect(database.url)
.then(() => {
    console.log("Successfully connected to the database");
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...');
    process.exit();
});



//rutas
const routes = require('./routes/routes.js');

app.use(routes);

//puerto que escucha
app.listen(3000, function(){
	console.log('Escuchando por el puerto 3000');
});