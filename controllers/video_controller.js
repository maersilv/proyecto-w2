const Video = require('../models/video_model.js');//como se llame la constante hereda a

//crea un nuevo video y lo agrega a la base de datos

exports.create = (req, res) => {
    var video = new Video();//aca y debe ser la misma
    video.id_user = req.body.id_user;
    video.name = req.body.name;
    video.url = req.body.url;

    video.save(function (err) {
        if (err) {
            res.send(err);
        }
        res.status(201);
        res.header({
            'location': `http://localhost/video/${video.id}`
        });
        res.json(video);
    });

};

//obtiene todos los datos
exports.getVideo = (req, res) => {
    var video = new Video();
    Video.find({ id_user: req.body.id_user })
        .then(video => {

            res.send(video);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Something went wrong while getting list of videos."
            });
        });
};

//obtiene todos los datos  por filtro
exports.filter = (req, res) => {
    
    Video.find({ name: {'$regex': req.body.text} })
        .then(video => {

            res.send(video);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Something went wrong while getting list of videos."
            });
        });
};


//obtiene los datos solicitados por id

exports.getVideoID = (req, res) => {
    const id = req.params.id;
    
    Video.findById(id, function (err, video) {
        if (err) {
            handleError(res, err, 500);
        }
        res.json(video);
    });
};

//actualizar videos por identificado por el ID
exports.update = (req, res) => {
    const id = req.params.id;
    var update = req.body;
    Video.findByIdAndUpdate(id, update, { new: true }, (err, video) => {
        if (err) return res.status(500).send({ message: 'Error internal server' });

        if (video) {
            return res.status(200).send({
                video
            });
        } else {
            return res.status(404).send({
                message: 'Not found video'
            });
        }


    });

};

//Eliminar videos identificando por el ID

exports.delete = (req, res) => {
    var video = new Video();//aca y debe ser la misma
    const id = req.params.id;

    Video.findByIdAndRemove(id, (err, video) => {
        if (err) {
            return res.status(500).send({ message: 'Error internal server' });
        }
        if (video) {
            return res.status(200).send({ video });


        } else {
            return res.status(404).send({
                message: 'Not found video'
            });
        }
    });
}
