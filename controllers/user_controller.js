const User = require('../models/user_model.js');//como se llame la constante hereda a
var Email = require('../email/email');
const Token = require('../models/TokenModel.js');
var JWT = require('jsonwebtoken');
//crea un nuevo usuario y lo agrega a la base de datos
exports.create = (req, res) => {



    var user = new User();//aca y debe ser la misma
    user.name = req.body.name;
    user.lastname = req.body.lastname;
    user.date_of_birth = req.body.date_of_birth;
    user.email = req.body.email;
    user.country = req.body.country;
    user.cellphone = req.body.cellphone;
    user.password = req.body.password;
    user.con_password = req.body.con_password;
    user.state = false;
    user.codeSms = 0;
    user.save(function (err) {
        if (err) {
            res.send(err);
        }
        res.status(201);
        Email.emailSend(user.email, "ermelindasilvaramirez@gmail.com", "Activacion de cuenta", "Mensaje texto", `<h1>Activar cuenta: http://localhost:3001/verification?token=${user.id}</h1>`);
        res.header({
            'location': `http://localhost/user/${user.id}`
        });
        res.json(user);
    });

};



//obtiene los datos de usuario
exports.getUser = (req, res) => {
    User.find(function (err, user) {
        if (err) {
            handleError(res, err, 500);
        }
        res.json(user);
    });
};

//actualizar usuarios 
exports.update = (req, res) => {
    const id = req.params.id;
    var update = req.body;
    User.findByIdAndUpdate(id, update, { new: true }, (err, user) => {
        if (err) return res.status(500).send({ message: 'Error internal server' });

        if (user) {
            return res.status(200).send({
                user
            });
        } else {
            return res.status(404).send({
                message: 'Not found user'
            });
        }


    });

};

//Eliminar usuarios

exports.delete = (req, res) => {
    const id = req.params.id;

    User.findByIdAndRemove(id, (err, user) => {
        if (err) {
            return res.status(500).send({ message: 'Error internal server' });
        }
        if (user) {
            return res.status(200).send({ user });

        } else {
            return res.status(404).send({
                message: 'Not found user'
            });
        }
    });
}

//login
exports.login = (req, res) => {
    const accountSid = 'AC686a72bda31800ef6b274073ae64f498';
    const authToken = '9f578ab6b2b9453e9c2701c9ae4771ef';
    const client = require('twilio')(accountSid, authToken);

    User.find({ email: req.body.email.value, password: req.body.password.value })
        .then(user => {
            if (Object.keys(user).length === 0) {
                return res.status(404).send({
                    message: "Usuario o contraseña invalidos"
                });
            } else {
                var codigo = Math.round(Math.random() * (2000 - 1000) + 1000);
                var update = {
                    codeSms: codigo
                };

                User.findByIdAndUpdate(user[0]._id, update, { new: true }, (err, user) => {
                    if (user) {
                        return res.status(200)
                    }
                });
                client.messages
                    .create({
                        to: '+50660615729',
                        from: '+19124000190',
                        body: 'Su codigo de acceso es ' + codigo

                    })

                    .then(message => res.json(message.sid)).catch(err => res.json(err.message));


                res.json(user);
            }
        }
        ).catch(
            err => {
                return res.status(400).send({
                    message: err.message
                });
            }
        )

};


//login second
exports.accessCode = (req, res) => {
    User.find({ email: req.body.email.value })
        .then(user => {
            if (req.body.codeSms.value == user[0].codeSms) {
                var data_user = {
                    _id: user[0]._id,
                    type: "admin"
                }
                var token = new Token();
                token.idUser = user[0]._id;
                token.token = JWT.sign({ data_user }, 'silva♫');
                token.save(function (err) {
                    if (err) {
                        res.send(err);
                    }
                    return res.status(200).send({
                        state: true,
                        token: token.token,
                        id_user: user[0]._id
                    });
                });

            } else {
                return res.status(200).send({
                    state: false
                });
            }
        }
        ).catch(
            err => {
                return res.status(400).send({
                    message: err.message
                });
            }
        )

};





