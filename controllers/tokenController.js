const Token = require('../models/TokenModel.js');
const kids = require('../models/profileModel');
var JWT = require('jsonwebtoken');

//se confirma la existencia del token
exports.confirmToken = (req, res, next) => {
    const headers = req.headers['authorization'];
    

    if (typeof headers !== 'undefined') {
       
        const header = headers.split(" ");
        Token.find({ token: header[1]})
            .then(token => {
               
                if (!token) {
                    return res.status(404).send({
                        message: "Token not found"
                    });
                }else{
                    return res.status(200).send({
                        token: header[1]
                    });
                }
            });

    }else{
        return res.status(404).send({
            message: "Token is required"
        });
    }

/*confirmar el token del lado del cliente en este caso los niños
    exports.loginConfirmKids=(req,res,next)=>{
        var user = req.params.user;
        kids.find({username:req.params.username,pin:req.params.pin})
        
    }*/
}