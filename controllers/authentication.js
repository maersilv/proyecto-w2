const User = require('../models/user_model.js');

//actualiza el estado de la cuenta
exports.update = (req, res) => {
    User.findByIdAndUpdate(
        req.body.token,
        {
            state: true
        }, { new: true }).then(user => {
            if (!user) {
                return res.status(404).send({
                    message: "user not found with id " + req.body.token
                });
            }
            res.send(user);
        })
        .catch(err => {
            if (err.kind === "ObjectId") {
                return res.status(404).send({
                    message: "user not found with id " + req.body.token
                });
            }
        
            return res.status(500).send({
                message: "Error updating user with id " + req.body.token
            });
        });
}