const Profile = require('../models/profileModel.js');//como se llame la constante hereda a
const Token = require('../models/TokenModel.js');
var JWT = require('jsonwebtoken');


//crea un nuevo perfiles y lo agrega a la base de datos
exports.create = (req, res) => {
    var profile = new Profile(); //aca y debe ser la misma
    profile.id_user = req.body.id_user;
    profile.name = req.body.name;
    profile.username = req.body.username;
    profile.pin = req.body.pin;
    profile.age = req.body.age;

    profile.save(function (err) {
        if (err) {
            res.send(err);
        }
        res.status(201);

        res.header({
            'location': `http://localhost/profile/${profile.id}`
        });
        res.json(profile);
    });


};



//obtiene los datos de perfiles

exports.getProfile = (req, res) => {
    var profile = new Profile();
    Profile.find({ id_user: req.body.id_user })
        .then(profile => {
            res.send(profile);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Something went wrong while getting list of users."
            });
        });
};
//actualizar videos por identificado por el ID
exports.getProfileID = (req, res) => {
    const id = req.params.id;

    Profile.findById(id, function (err, profile) {
        if (err) {
            handleError(res, err, 500);
        }
        res.json(profile);

    });
};

//actualizar perfiles 
exports.update = (req, res) => {
    const id = req.params.id;
    var update = req.body;
    Profile.findByIdAndUpdate(id, update, { new: true }, (err, profile) => {
        if (err) return res.status(500).send({ message: 'Error internal server' });

        if (profile) {
            return res.status(200).send({
                profile
            });
        } else {
            return res.status(404).send({
                message: 'Not found profile'
            });
        }


    });

};

//Eliminar perfiles

exports.delete = (req, res) => {
    var profile = new Profile();//aca y debe ser la misma
    const id = req.params.id;

    Profile.findByIdAndRemove(id, (err, profile) => {
        if (err) {
            return res.status(500).send({ message: 'Error internal server' });
        }
        if (profile) {
            return res.status(200).send({ profile });

        } else {
            return res.status(404).send({
                message: 'Not found profile'
            });
        }
    });
};
//login de cliente en este caso niños
exports.login = (req, res) => {
   
    Profile.find({ username: req.body.username.value, pin: req.body.pin.value 
    })
    .then(profile => {
        
        if (Object.keys(profile).length === 0) {
            return res.status(404).send({
                
                message: "Usuario o pin invalidos"
            });
        }else{
            var token = new Token();
            token.idUser = profile[0]._id;
            token.token = JWT.sign({profile},'silva♫');
            token.save(function(err){
                if(err) {
                    res.send(err);
                }
                return res.status(200).send({
                    state: true,
                    token:token.token,
                    id_user: profile[0].id_user
                });
            });
           
        }
    }).catch(
        err => {
            return res.status(400).send({
                message: err.message
            });
        }
    )

};




