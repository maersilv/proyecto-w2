var express = require('express');
const router = express.Router();

const Video = require('../controllers/video_controller.js');
const User = require('../controllers/user_controller.js');
const auth = require('../controllers/authentication.js');
const Profile = require('../controllers/profileController.js');
const Token = require('../controllers/tokenController.js');
const MiddlewareVideos = require('../middleware/middleware_videos.js');
const MiddlewareUsers = require('../middleware/middleware_users.js');
const MiddlewareProfile = require('../middleware/middleware_profile.js');

//routes de video
router.post('/video',MiddlewareVideos.prueba,MiddlewareVideos.validateVideos, Video.create);
router.post('/video/get', Video.getVideo);
router.post('/video/search',Video.filter);
router.get('/video/:id', Video.getVideoID);
router.patch('/video/:id', Video.update);
router.delete('/video/:id', Video.delete);

//ruta de usuarios
router.post('/user',MiddlewareUsers.dataRequired, User.create);
router.get('/user', User.getUser);
router.patch('/user/:id', User.update);
router.delete('/user/:id', User.delete);


//ruta de perfiles
router.post('/profile',MiddlewareProfile.dataRequired, Profile.create);
router.post('/profile/get', Profile.getProfile);
router.get('/profile/:id', Profile.getProfileID);
router.patch('/profile/:id', Profile.update);
router.delete('/profile/:id', Profile.delete);

//login
router.post('/login',User.login);
router.post('/code',User.accessCode);


router.post('/authentication', auth.update);

//ruta token
router.get('/token_confirm', Token.confirmToken);
//login cliente
router.post('/loginC',Profile.login);

module.exports = router;
