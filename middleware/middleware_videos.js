exports.validateVideos = (req, res, next) => {
    var error = null;
    if (typeof req.body.name === 'undefined') {
       error = "el name es requerido";
    } 

    if (typeof req.body.url === 'undefined') {
        error = "la url es requerida";
    } 

    if (error !== null) {
        return res.status(400).send({
            message: error
        });
    }else{
        next();
    }

}

exports.prueba = (req, res, next) => {
    next();

}