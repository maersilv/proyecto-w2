

exports.dataRequired = (req, res, next) => {
    var error = null;

    if (typeof req.body.name === 'undefined') {
        return res.status(400).send({
            message: "el nombre es requerida"
        });
    }
    if (req.body.name == '') {
        return res.status(400).send({
            message: "El campo de nombre esta vacio"
        });
    }

    if (typeof req.body.lastname === 'undefined') {
        return res.status(400).send({
            message: "el apellido es requerida"
        });
    }
    if (req.body.lastname == '') {
        return res.status(400).send({
            message: "El campo de apellido esta vacio"
        });
    }


    if (typeof req.body.email === 'undefined') {
        return res.status(400).send({
            message: "el correo es requerido"
        });
    }
    if (req.body.email == '') {
        return res.status(400).send({
            message: "el campo del correo esta vacio "
        });
    }
    if (!(/^\w+([\.-]?\w+)@\w+([\.-]?\w+)(\.\w{2,4})+$/.test(req.body.email))) {
        return res.status(400).send({
            message: "el correo ingresado no es valido"
        });
    }
    if (typeof req.body.cellphone === 'undefined') {
        return res.status(400).send({
            message: "el numero de celular es requerido"
        });
    }
    if (req.body.cellphone == '') {
        return res.status(400).send({
            message: "el campo de numero telefonico esta vacio"
        });
    }
    if (typeof req.body.password === 'undefined') {

        return res.status(400).send({
            message: "la contraseña es requerida"
        });
    }
    if (req.body.password == '') {

        return res.status(400).send({
            message: "el campo de contraseña esta vacio"
        });
    }
    if (typeof req.body.con_password === 'undefined') {

        return res.status(400).send({
            message: "Es necesario que digite la confirmacion de la contraseña"
        });
    }
    if (req.body.con_password == '') {

        return res.status(400).send({
            message: "el campo de confirmar contraseña esta vacio"
        });
    }
    if (typeof req.body.date_of_birth === 'undefined') {

        return res.status(400).send({
            message: "La fecha de nacimiento es requerida"
        });
    }


    if (/^(\d{1,2})[/](\d{1,2})[/](\d{3,4})$/.exec(req.body.date_of_birth)) {

        return res.status(400).send({
            message: "formato de fecha invalido"
        });
    }
    if (req.body.password != req.body.con_password) {

        return res.status(400).send({
            message: "Las contraseñas deben de coincidir"
        });

    }



    next();

}



